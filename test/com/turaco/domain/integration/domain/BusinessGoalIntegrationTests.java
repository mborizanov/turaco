package com.turaco.domain.integration.domain;

import com.hp.hpl.jena.ontology.Individual;
import com.turaco.dal.BusinessOntology;
import com.turaco.domain.BusinessGoal;
import com.turaco.domain.BusinessMethod;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: PC
 * Date: 11/11/13
 * Time: 4:14 PM
 * To change this template use File | Settings | File Templates.
 */

public class BusinessGoalIntegrationTests {
    private BusinessGoal businessGoal;
    private List<BusinessMethod> businessMethodsList;

    @Before
    public void setUp() throws Exception {
        Individual businessGoalIndividual = BusinessOntology.getIndividualByLocalName("FinancialAnalysis");
        businessGoal = new BusinessGoal(businessGoalIndividual);
        businessMethodsList = businessGoal.listBusinessMethods();
    }

    @Test
    public void listBusinessMethods_RealData_ShouldBeCorrectNumber() {
        assertEquals(4, businessMethodsList.size());
    }

    @Test
    public void instance_RealData_ShouldNotBeNull() {
        assertNotNull(businessGoal);
        Assert.assertEquals("Financial Analysis", businessGoal.getName());
    }
}
