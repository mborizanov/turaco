package com.turaco.domain.integration.domain;

import com.hp.hpl.jena.ontology.Individual;
import com.turaco.dal.BusinessOntology;
import com.turaco.domain.BusinessMethod;
import com.turaco.domain.BusinessTask;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.InvalidPropertiesFormatException;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: PC
 * Date: 11/11/13
 * Time: 6:22 PM
 * To change this template use File | Settings | File Templates.
 */

public class BusinessTaskIntegrationTests {

    private Individual businessTaskIndividual;
    private Individual businessMethodIndividual;
    private BusinessTask businessTask;
    private BusinessMethod businessMethod;
    private List<BusinessMethod> businessMethodList;

    @Before
    public void given_when() throws Exception {
        businessTaskIndividual = BusinessOntology.getIndividualByLocalName("FinancialAnalysis.AnalyseProfitability.CalculateCashFlowMargin");
        businessMethodIndividual = BusinessOntology.getIndividualByLocalName("FinancialAnalysis.AnalyseProfitability");
        businessTask = new BusinessTask(businessTaskIndividual);
        businessMethod = new BusinessMethod(businessMethodIndividual);
        businessMethodList = businessTask.listBusinessMethods();
    }

    @Test
    public void instance_RealData_ShouldNotBeNull(){
        assertNotNull(businessTask);
    }

    @Test
    public void getName_RealData_ShouldBeCorrect() throws Exception {
        Assert.assertEquals("Calculate Cash Flow Margin", businessTask.getName());
    }

    @Test
    public void getBusinessMethodList_ReadData_ShouldBeCorrectSize() {
        assertEquals(1,businessMethodList.size());
    }

    @Test
    public void getBusinessMethodName_RealData_ShouldBeCorrect() throws InvalidPropertiesFormatException {
        Assert.assertEquals("Analyse Profitability", businessTask.getBusinessMethodName());
    }

    @Test
    public void getBusinessGoalName_MockData_ShouldReturnBusinessGoalName(){
        assertEquals("Financial Analysis",businessTask.getBusinessGoalName());
    }

    @Test
    public void getBusinessMethod_MockData_ShouldReturnCorrectBusinessMethods() throws InvalidPropertiesFormatException {
        assertEquals(businessMethod.getName(),businessTask.getBusinessMethod().getName());
        assertEquals(businessMethod.getComment(),businessTask.getBusinessMethod().getComment());
    }

    @Test
    public void getIndividual_MockData_ShouldReturnCorrectIndividual(){
        assertEquals(businessTaskIndividual ,businessTask.getIndividual());
    }
}
