package com.turaco.domain.integration.domain;

import com.hp.hpl.jena.ontology.Individual;
import com.turaco.dal.BusinessOntology;
import com.turaco.domain.BusinessGoal;
import com.turaco.domain.BusinessMethod;
import com.turaco.domain.BusinessTask;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: PC
 * Date: 11/11/13
 * Time: 5:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class BusinessMethodIntegrationTests {

    private Individual businessMethodIndividual;
    private BusinessMethod businessMethod;
    private List<BusinessTask> businessTaskList;
    private List<BusinessGoal> businessGoals;

    @Before
    public void setUp() throws Exception {
        businessMethodIndividual = BusinessOntology.getIndividualByLocalName("FinancialAnalysis.AnalyseProfitability");
        businessMethod = new BusinessMethod(businessMethodIndividual);
        businessGoals = businessMethod.listBusinessGoals();
        businessTaskList = businessMethod.listBusinessTasks();
    }

    @Test
    public void listBusinessGoals_RealData_ShouldReturnCorrectBusinessGoal() {
        assertEquals(1,businessGoals.size());
        Assert.assertEquals("Financial Analysis", businessGoals.get(0).getName());
    }

    @Test
    public void listBusinessTasks_RealData_ShouldReturnCorrectNumberOfBusinessTasks() {
        assertEquals(7,businessTaskList.size());
    }

    @Test
    public void instance_RealData_ShouldNotBeNull() {
        assertNotNull(businessMethod);
    }
}
