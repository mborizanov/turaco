package com.turaco.domain.integration.util;

import com.hp.hpl.jena.ontology.Individual;
import com.turaco.dal.BusinessOntology;
import com.turaco.domain.BusinessMethod;
import com.turaco.domain.BusinessTask;
import com.turaco.util.OperationalTasksReport;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * Author: Martin Borizanov
 * Date: 3/5/14
 */
public class OperationalTaskReportIntegrationTests {

    /*
    Very Important: These tests create a PDF file E:/file.pdf
     */

    OperationalTasksReport report;

    @Before
    public void setUp() throws Exception {

        Individual businessMethodIndividual = BusinessOntology.getIndividualByLocalName("FinancialAnalysis.AnalyseProfitability");
        BusinessMethod businessMethod = new BusinessMethod(businessMethodIndividual);
        List<BusinessTask> businessTaskList = businessMethod.listBusinessTasks();

        report = new OperationalTasksReport(businessTaskList,"E://file");
    }

    @Test
    public void Report_RealData_ShouldNotBeNull() {
        assertNotNull(report);
    }
}
