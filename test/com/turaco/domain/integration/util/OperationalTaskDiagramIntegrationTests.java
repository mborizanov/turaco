package com.turaco.domain.integration.util;

import com.hp.hpl.jena.ontology.Individual;
import com.turaco.dal.BusinessOntology;
import com.turaco.domain.BusinessMethod;
import com.turaco.domain.BusinessTask;
import com.turaco.util.OperationalTaskDiagram;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * Author: Martin Borizanov
 * Date: 3/5/14
 */
public class OperationalTaskDiagramIntegrationTests {

    OperationalTaskDiagram diagram;

    @Before
    public void setUp() throws Exception {
        Individual businessMethodIndividual = BusinessOntology.getIndividualByLocalName("FinancialAnalysis.AnalyseProfitability");
        BusinessMethod businessMethod = new BusinessMethod(businessMethodIndividual);
        List<BusinessTask> businessTaskList = businessMethod.listBusinessTasks();
        diagram = new OperationalTaskDiagram(businessTaskList);
        diagram.setSize(diagram.getDiagramWidth(), diagram.getDiagramHeight());
    }

    @Test
    public void Diagram_RealData_ShouldNotBeNull() {
        assertNotNull(diagram);
    }

    @Test
    public void Diagram_RealData_ShouldHaveCorrectDimensions(){
        assertEquals(diagram.getSize(), new Dimension(diagram.getDiagramWidth(), diagram.getDiagramHeight()));
    }
}
