package com.turaco.domain.unit.domain;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.turaco.domain.BusinessGoal;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class BusinessGoalUnitTests {

    Individual mockIndividual;
    private BusinessGoal businessGoal;

    @Before
    public void setUp() throws Exception {

        mockIndividual = mock(Individual.class);
        OntClass mockOntClass = mock(OntClass.class);
        StmtIterator mockStmtIterator = mock(StmtIterator.class);

        when(mockIndividual.getOntClass()).thenReturn(mockOntClass);
        when(mockOntClass.getLocalName()).thenReturn("BusinessGoal");
        when(mockIndividual.listProperties()).thenReturn(mockStmtIterator);
        when(mockIndividual.getLabel(null)).thenReturn("Mocked Goal");

        businessGoal = new BusinessGoal(mockIndividual);
    }

    @Test
    public void BusinessGoal_MockData_ShouldNotBeNull() throws Exception {
        assertNotNull(businessGoal);
    }

    @Test
    public void getName_MockData_ShouldBeCorrect() {
        assertEquals("Mocked Goal", businessGoal.getName());
    }

    @Test
    public void listBusinessGoals_MockData_ShouldNotListBusinessGoals() throws Exception {
        assertEquals(0, businessGoal.listBusinessMethods().size());
    }
}
