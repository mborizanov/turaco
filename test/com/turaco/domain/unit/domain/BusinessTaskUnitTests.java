package com.turaco.domain.unit.domain;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.turaco.domain.BusinessMethod;
import com.turaco.domain.BusinessTask;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.InvalidPropertiesFormatException;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created with IntelliJ IDEA.
 * User: PC
 * Date: 11/11/13
 * Time: 6:22 PM
 * To change this template use File | Settings | File Templates.
 */

public class BusinessTaskUnitTests {

    Individual mockIndividual;
    private BusinessTask businessTask;
    private List<BusinessMethod> businessMethodList;
    private BusinessMethod mockBusinessMethod;

    @Before
    public void setUp() throws Exception {

        mockIndividual = mock(Individual.class);
        mockBusinessMethod = mock(BusinessMethod.class);
        OntClass mockOntClass = mock(OntClass.class);
        List<BusinessMethod> mockMethodsList = mock(List.class);
        StmtIterator mockStmtIterator = mock(StmtIterator.class);

        when(mockIndividual.getOntClass()).thenReturn(mockOntClass);
        when(mockIndividual.listProperties()).thenReturn(mockStmtIterator);
        when(mockIndividual.getLabel(null)).thenReturn("Mocked Task");
        when(mockOntClass.getLocalName()).thenReturn("BusinessTask");

        businessTask = new BusinessTask(mockIndividual);
        businessMethodList = businessTask.listBusinessMethods();
    }

    @Test
    public void BusinessTask_MockData_ShouldNotBeNull() throws Exception {
        assertNotNull(businessTask);
        Assert.assertEquals("Mocked Task", businessTask.getName());
    }

    @Test
    public void listBusinessMethods_MockData_ShouldNotReturnBusinessMethods() {
        assertEquals(0, businessMethodList.size());
    }

    @Ignore
    @Test
    public void getBusinessGoalName_MockData_ShouldReturnBusinessGoalName(){
        assertEquals("Mock Business Goal",businessTask.getBusinessGoalName());
    }

    @Ignore
    @Test
    public void getBusinessMethod_MockData_ShouldReturnCorrectBusinessMethods() throws InvalidPropertiesFormatException {
        assertSame(mockBusinessMethod, businessTask.getBusinessMethod());
    }

    @Test
    public void getIndividual_MockData_ShouldReturnCorrectIndividual(){
        assertEquals(mockIndividual,businessTask.getIndividual());
    }
}
