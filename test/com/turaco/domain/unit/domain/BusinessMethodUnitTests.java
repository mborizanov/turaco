package com.turaco.domain.unit.domain;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.turaco.domain.BusinessMethod;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class BusinessMethodUnitTests {

    Individual mockIndividual;
    private BusinessMethod businessMethod;

    @Before
    public void setUp() throws Exception {

        mockIndividual = mock(Individual.class);
        OntClass mockOntClass = mock(OntClass.class);
        StmtIterator mockStmtIterator = mock(StmtIterator.class);

        when(mockIndividual.getOntClass()).thenReturn(mockOntClass);
        when(mockOntClass.getLocalName()).thenReturn("BusinessMethod");
        when(mockIndividual.listProperties()).thenReturn(mockStmtIterator);
        when(mockIndividual.getLabel(null)).thenReturn("Mocked Method");
        when(mockIndividual.getComment(null)).thenReturn("Mocked Method");

        businessMethod = new BusinessMethod(mockIndividual);
    }

    @Test
    public void BusinessMethod_MockData_ShouldNotBeNull() throws Exception {
        assertNotNull(businessMethod);
    }

    @Test
    public void getName_MockData_ShouldBeCorrect() {
        Assert.assertEquals("Mocked Method", businessMethod.getName());
    }

    @Test
    public void getComment_MockData_ShouldBeCorrect() {
        Assert.assertEquals("Mocked Method", businessMethod.getComment());
    }

    @Test
    public void listBusinessGoals_MockData_ShouldNotListBusinessGoals() throws Exception {
        assertEquals(0, businessMethod.listBusinessGoals().size());
    }
}
