package com.turaco.domain.unit.util;

import com.turaco.domain.BusinessTask;
import com.turaco.util.OperationalTaskDiagram;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.Iterator;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/*
Naming conventions: http://osherove.com/blog/2005/4/3/naming-standards-for-util-tests.html
 */

public class OperationalTaskDiagramUnitTests {

    OperationalTaskDiagram diagram;

    @Before
    public void setUp() throws Exception {

        List<BusinessTask> mockList = mock(List.class);
        BusinessTask mockBusinessTask = mock(BusinessTask.class);
        Iterator<BusinessTask> mockTaskIterator = mock(Iterator.class);

        mockList.add(mockBusinessTask);
        when(mockList.get(0)).thenReturn(mockBusinessTask);
        when(mockList.iterator()).thenReturn(mockTaskIterator);
        when(mockBusinessTask.getBusinessGoalName()).thenReturn("Test Business Goal");
        when(mockBusinessTask.getBusinessMethodName()).thenReturn("Test Business Method");

        diagram = new OperationalTaskDiagram(mockList);
        diagram.setSize(diagram.getDiagramWidth(), diagram.getDiagramHeight());
    }

    @Test
    public void Diagram_MockData_ShouldNotBeNull() {
        assertNotNull(diagram);
    }

    @Test
    public void Diagram_MockData_ShouldHaveCorrectDimensions(){
          assertEquals(diagram.getSize(), new Dimension(diagram.getDiagramWidth(), diagram.getDiagramHeight()));
    }
}
