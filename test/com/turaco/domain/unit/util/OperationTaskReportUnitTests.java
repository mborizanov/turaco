package com.turaco.domain.unit.util;

import com.turaco.domain.BusinessMethod;
import com.turaco.domain.BusinessTask;
import com.turaco.util.OperationalTasksReport;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created with IntelliJ IDEA.
 * Author: Martin Borizanov
 * Date: 3/5/14
 */
public class OperationTaskReportUnitTests {

    /*
    Very Important: These tests create a PDF file E:/file.pdf
     */

    OperationalTasksReport report;

    @Before
    public void setUp() throws Exception {

        List<BusinessTask> mockList = mock(List.class);
        BusinessTask mockBusinessTask = mock(BusinessTask.class);
        BusinessMethod mockBusinessMethod = mock(BusinessMethod.class);
        Iterator<BusinessTask> mockTaskIterator = mock(Iterator.class);

        mockList.add(mockBusinessTask);
        when(mockList.get(0)).thenReturn(mockBusinessTask);
        when(mockList.iterator()).thenReturn(mockTaskIterator);
        when(mockBusinessTask.getBusinessGoalName()).thenReturn("Test Business Goal");
        when(mockBusinessTask.getBusinessMethodName()).thenReturn("Test Business Method");
        when(mockBusinessTask.getBusinessMethod()).thenReturn(mockBusinessMethod);
        when(mockBusinessTask.getBusinessMethod().getComment()).thenReturn("Test Business Method");


        report = new OperationalTasksReport(mockList,"E://file");
    }

    @Test
    public void Report_MockData_ShouldNotBeNull() {
        assertNotNull(report);
    }
}
