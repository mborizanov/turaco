package com.turaco.util;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import com.turaco.domain.BusinessTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.Iterator;
import java.util.List;


public class OperationalTaskDiagram extends JFrame
{

    private static Logger logger = LogManager.getLogger(OperationalTaskDiagram.class.getName());
    public final URL IMAGE_WIDE_RHOMBUS_SHAPE = getClass().getResource("/parall.png");
    public  final String VERTEX_STYLE_METHODS_AND_TASKS = String.format("shape=image;image=" + IMAGE_WIDE_RHOMBUS_SHAPE.toString());
    private int diagramHeight = 400;
    mxGraph graph = new mxGraph();

    /**
     * Returns the diagram height.
     *
     * @return the diagram height.
     */
    public int getDiagramHeight(){
        return  diagramHeight;
    }

    /**
     * Returns the diagram width
     *
     * @return the diagram width
     */
    public int getDiagramWidth(){
        logger.entry();
        return logger.exit(DiagramConstants.MAXIMUM_DIAGRAM_WIDTH + 150);
    }

    /**
     * Constructs an Operational Task Diagram from a list of BusinessTask instances
     *
     * @param businessTasks The list of Business Task instances
     * @see com.turaco.util.BusinessTaskIterator
     * @see com.turaco.domain.BusinessTask
     */
    public OperationalTaskDiagram(List<BusinessTask> businessTasks){
        super("Operational Task KAOS Diagram");
        logger.entry();

        Object parent = graph.getDefaultParent();
        graph.getModel().beginUpdate();
        try
        {
            Object businessGoal = graph.insertVertex(parent, null, getBusinessGoalName(businessTasks),
                    DiagramConstants.DIAGRAM_EDGE_X_COORDINATE,
                    DiagramConstants.DIAGRAM_EDGE_Y_COORDINATE,
                    DiagramConstants.VERTEX_WIDTH,
                    DiagramConstants.VERTEX_HEIGHT,
                    DiagramConstants.VERTEX_STYLE_BUSINESS_GOAL);

            Object businessMethod = graph.insertVertex(parent, null, getBusinessMethodName(businessTasks),
                    DiagramConstants.DIAGRAM_EDGE_X_COORDINATE,
                    DiagramConstants.DIAGRAM_EDGE_Y_COORDINATE + 100,
                    DiagramConstants.VERTEX_WIDTH,
                    DiagramConstants.VERTEX_HEIGHT,
                    VERTEX_STYLE_METHODS_AND_TASKS);

            graph.insertEdge(graph.getDefaultParent(), null,
                    DiagramConstants.RELATIONSHIP_HAS_A_BUSINESS_METHOD, businessGoal, businessMethod);

            GenerateVerticesAndEdges(businessMethod, businessTasks, graph);
        } catch (Exception e) {
            logger.catching(e);
        } finally {
            graph.getModel().endUpdate();
        }

        mxGraphComponent graphComponent = new mxGraphComponent(graph);
        getContentPane().add(graphComponent);

        logger.exit();
    }

    private String getBusinessMethodName(List<BusinessTask> businessTasks) throws InvalidPropertiesFormatException {
        logger.entry();
        return logger.exit(businessTasks.get(0).getBusinessMethodName());
    }

    private String getBusinessGoalName(List<BusinessTask> businessTasks) throws Exception {
        logger.entry();
        return logger.exit(businessTasks.get(0).getBusinessGoalName());
    }

    private void GenerateVerticesAndEdges(Object parent, List<BusinessTask> tasks, mxGraph graph){
        logger.entry();

        ArrayList<Object> listOfVertices = new ArrayList<>();
        Iterator<BusinessTask> taskIterator = tasks.iterator();


        int xCoordinate = 20;
        int yCoordinate = 250;

        while(taskIterator.hasNext()){

            String task = taskIterator.next().getName();
            if(xCoordinate >= DiagramConstants.MAXIMUM_DIAGRAM_WIDTH){
                xCoordinate = 20;
            }
            listOfVertices.add(graph.insertVertex(graph.getDefaultParent(), null, task, xCoordinate, yCoordinate,
                    DiagramConstants.VERTEX_WIDTH,
                    DiagramConstants.VERTEX_HEIGHT,
                    VERTEX_STYLE_METHODS_AND_TASKS));

            xCoordinate += 300;
            yCoordinate += 50;
            diagramHeight += 50;
        }
        GenerateEdges(parent, listOfVertices, graph);

        logger.exit();
    }

    /**
     * Exports the contents of a JFrame to a PNG file.
     *
     * @param frame the JFrame
     * @param fileName the File name
     * @param fileDirectory the File directory
     */
    public static void exportToPng(JFrame frame, String fileName, File fileDirectory )  {
        logger.entry();

        try {
            int width = frame.getContentPane().getWidth();
            int height = frame.getContentPane().getHeight();

            BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2d = img.createGraphics();
            frame.printAll(g2d);
            ImageIO.write(img.getSubimage(9,31, width -9, height -31),"png", new File(fileDirectory.getAbsolutePath()+"/"+fileName+".png"));
            g2d.dispose();
        }
        catch(Exception e)  {
            logger.catching(e);
        }

        logger.exit();
    }

    /**
     * Exports the contents of a JFrame to a PNG file.
     *
     * @param frame the JFrame
     * @param absoluteFileDirectory the absolute File directory (File directory +  Filename)
     */
    public static void exportToPng(JFrame frame, String absoluteFileDirectory)  {
        logger.entry();

        try {
            int width = frame.getContentPane().getWidth();
            int height = frame.getContentPane().getHeight();

            BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2d = img.createGraphics();
            frame.printAll(g2d);
            ImageIO.write(img.getSubimage(9,31, width -9, height -31),"png", new File(absoluteFileDirectory +".png"));
            g2d.dispose();
        }
        catch(Exception e)  {
            logger.catching(e);
        }

        logger.exit();
    }

    private void GenerateEdges(Object parent, ArrayList<Object> vertices, mxGraph graph) {
        logger.entry();

        Object[] vertexIterator = vertices.toArray();
        for (int index = 0; index < vertexIterator.length; index++){
            Object vertex = vertexIterator[index];
            if (index+1 < vertexIterator.length){
                Object nextVertex = vertexIterator[index+1];
                graph.insertEdge(graph.getDefaultParent(),null,DiagramConstants.RELATIONSHIP_IS_FOLLOWED_BY,vertex,nextVertex);
            }
            graph.insertEdge(graph.getDefaultParent(),null, DiagramConstants.RELATIONSHIP_HAS_TASK, parent, vertex, DiagramConstants.EDGE_STYLE_HAS_TASK);
        }

        logger.exit();
    }
}