package com.turaco.util;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.rdf.model.Statement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Martin Borizanov
 * Date: 11/15/13
 */
public class OntologyUtility {
    private static Logger logger = LogManager.getLogger(ClassLoader.class.getName());

    public OntologyUtility() {
    }


    /**
     * Lists the ontology properties of an Individual
     *
     * @param individual  The individual
     * @return List of the <a href="http://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/rdf/model/Statement.html">Statement</a> instances modeling the properties of the Individual
     */
    public static List<Statement> ListPropertyOfIndividual(Individual individual) {
        logger.entry();
        return logger.exit(individual.listProperties().toList());
    }
}
