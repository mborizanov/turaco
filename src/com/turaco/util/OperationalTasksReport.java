package com.turaco.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfWriter;
import com.turaco.domain.BusinessTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Martin Borizanov
 * Date: 2/1/14
 */

public class OperationalTasksReport {

    private static Logger logger = LogManager.getLogger(OperationalTasksReport.class.getName());

    /**
     * Generates an Operational Task Report from a List of Business Task instances ,
     * and saves it as a PDF to a specified file.
     *
     * @param businessTasks the Business Task Instances List
     * @param absoluteFileDirectory the absolute File directory (File directory +  Filename)
     * @see com.turaco.domain.BusinessTask
     * @see com.turaco.util.BusinessTaskIterator
     */
    public OperationalTasksReport(List<BusinessTask> businessTasks, String absoluteFileDirectory){
        logger.entry();

        Document document = new Document();

        try {
            WriteToPdf(businessTasks, document, new FileOutputStream(absoluteFileDirectory + ".pdf"));
        } catch (FileNotFoundException e) {
            logger.catching(e);
        }

        logger.exit();
    }

    private void setDocumentHeader(Document document, PdfWriter writer) throws DocumentException, IOException {
        logger.entry();

        PdfContentByte contentByte = writer.getDirectContent();
        contentByte.saveState();

        document.add(new Paragraph(Chunk.NEWLINE));
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);

        try {

            URL bannerImageURL = getClass().getResource("/banner.jpg");
            URL whiteLogoImageURL = getClass().getResource("/logoWhite.png");
            URL uomLogoImageURL = getClass().getResource("/uomLogo.png");

            Image turacoLogo = com.itextpdf.text.Image.getInstance(whiteLogoImageURL);
            turacoLogo.scaleAbsolute(78, 30);
            turacoLogo.setAbsolutePosition(document.getPageSize().getWidth() - 78, document.getPageSize().getHeight() - 30);

            Image uomLogo = com.itextpdf.text.Image.getInstance(uomLogoImageURL);
            uomLogo.scaleAbsolute(70, 30);
            uomLogo.setAbsolutePosition(3, document.getPageSize().getHeight() - 33);

            Image banner = com.itextpdf.text.Image.getInstance(bannerImageURL);
            banner.scaleAbsolute(document.getPageSize().getWidth(),120);
            banner.setAbsolutePosition(0, document.getPageSize().getHeight() - 120);

            contentByte.addImage(banner);
            contentByte.addImage(turacoLogo);
            contentByte.addImage(uomLogo);

        } catch (IOException e) {
            logger.catching(e);
        }

        PdfGState gState = new PdfGState();
        gState.setFillOpacity(0.5f);
        contentByte.setGState(gState);
        contentByte.setColorFill(BaseColor.BLACK);
        contentByte.rectangle(0,document.getPageSize().getHeight()-93, document.getPageSize().getWidth()-200,50);
        contentByte.fill();
        contentByte.restoreState();

        BaseFont font = BaseFont.createFont(BaseFont.HELVETICA_BOLDOBLIQUE,BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        String heading = "Operational Task Report";
        contentByte.saveState();
        contentByte.beginText();
        contentByte.moveText(15, document.getPageSize().getHeight() - 75);
        contentByte.setFontAndSize(font, 27);
        contentByte.setColorFill(BaseColor.WHITE);
        contentByte.showText(heading);
        contentByte.endText();
        contentByte.restoreState();

        logger.exit();
    }

    private void WriteToPdf(List<BusinessTask> businessTasks, Document document, FileOutputStream fileOutputStream) {
        logger.entry();

        try {
            PdfWriter writer = PdfWriter.getInstance(document, fileOutputStream);
            document.open();
            setDocumentHeader(document, writer);

            Font headlineFont = FontFactory.getFont(FontFactory.HELVETICA, 27);
            Font commentFont = FontFactory.getFont(FontFactory.HELVETICA,10,Font.ITALIC, BaseColor.GRAY);

            Chunk headline = new Chunk(businessTasks.get(0).getBusinessMethodName()+"\n\n",headlineFont);
            Paragraph comment = new Paragraph(businessTasks.get(0).getBusinessMethod().getComment(),commentFont);
            comment.setLeading((float) 1.5,1);

            document.add(headline);
            document.add(comment);
            document.add(Chunk.NEWLINE);
            document.add(Chunk.NEWLINE);

            for (int index = 0; index < businessTasks.size();index++){
                Paragraph paragraph = new Paragraph();
                paragraph = new Paragraph((index+1) + ". " + businessTasks.get(index).getName() + "\n");
                document.add(paragraph);
                paragraph = new Paragraph(businessTasks.get(index).getComment(),commentFont);
                paragraph.setLeading((float) 1.5 ,1);
                document.add(paragraph);
                document.add(Chunk.NEWLINE);
            }

            document.close();
        } catch (DocumentException | IOException e) {
            logger.catching(e);
        }

        logger.exit();
    }
}
