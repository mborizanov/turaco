package com.turaco.util;

/**
 * Created with IntelliJ IDEA.
 * Author: Martin Borizanov
 * Date: 2/13/14
 */
public class DiagramConstants {
    public static final int MAXIMUM_DIAGRAM_WIDTH = 1600;
    public static final int DIAGRAM_EDGE_X_COORDINATE = 100;
    public static final int DIAGRAM_EDGE_Y_COORDINATE = 10;
    public static final String VERTEX_STYLE_BUSINESS_GOAL = "shape=cloud;whiteSpace=wrap;";
    public static final String EDGE_STYLE_HAS_TASK = "fontColor=#999;strokeColor=#999;dashed=true;";
    public static final String RELATIONSHIP_HAS_A_BUSINESS_METHOD = "has a business method";
    public static final String RELATIONSHIP_IS_FOLLOWED_BY = "is followed by";
    public static final String RELATIONSHIP_HAS_TASK = "has task";
    public static final int VERTEX_WIDTH = 200;
    public static final int VERTEX_HEIGHT = 35;
}
