package com.turaco.util;

import com.hp.hpl.jena.rdf.model.Statement;
import com.turaco.domain.BusinessTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Provides interface for successive iteration operations on Business Task collections,
 * via concrete implementation of the java.util.Iterator interface
 *
 * @see java.util.Iterator
 * @see com.turaco.domain.BusinessTask
 */
public class BusinessTaskIterator implements Iterator {

    private List<BusinessTask> taskList;
    private int position;
    private int size;
    private static Logger logger = LogManager.getLogger(ClassLoader.class.getName());

    /**
     * Constructs BusinessTaskIterator
     *
     * @param tasks The BusinessTask List collection
     * @throws Exception Exception propagated from the BusinessTask.getIsFollowedBy method.
     * @see com.turaco.domain.BusinessTask#getIsFollowedBy()
     */
    public BusinessTaskIterator(List<BusinessTask> tasks) throws Exception {
        logger.entry();

        position = 0;
        size = tasks.size();

        SortTasks(tasks);

        logger.exit();
    }


    /**
     * Checks if there is next element in the Iterator's collection.
     *
     * @return Boolean value representing whether there is a next element.
     */
    @Override
    public boolean hasNext() {
        logger.entry();
        return logger.exit((position < size));
    }

    /**
     * Returns the next element in the Iterator's collection.
     *
     * @returns the next element in the Iterator collection.
     */
    @Override
    public BusinessTask next() {
        logger.entry();

        if(hasNext()){
            BusinessTask next = taskList.get(position);
            position ++;
            return logger.exit(next);
        }
        else {
            throw new IndexOutOfBoundsException("This is the last element of the iteration!");
        }
    }

    /**
     * Unsupported functionality
     */
    @Override
    public void remove() {
        throw new UnsupportedOperationException("Tasks cannot be remove via Iterator!");
    }

    /**
     * Returns the Iterator List collection
     *
     * @return the Iterator List collection
     */
    public List<BusinessTask> toList(){
        logger.entry();
        return logger.exit(taskList);
    }

    private void SortTasks(List<BusinessTask> tasks) throws Exception {
        logger.entry();

        taskList = new ArrayList<>();
        BusinessTask firstTask = FindFirstTask(tasks);
        taskList.add(firstTask);
        FollowTaskChainFromFirstTask(firstTask);

        logger.exit();
    }

    private BusinessTask FindFirstTask(List<BusinessTask> tasks) {
        logger.entry();

        for(BusinessTask task : tasks)
        {
            boolean isFirst = true;
            List<Statement> taskProperties = OntologyUtility.ListPropertyOfIndividual(task.asIndividual());
            for(Statement property : taskProperties){
                if(property.getPredicate().getLocalName().toLowerCase().contains("follows")) {

                    isFirst = false;
                }
            }
            if(isFirst) return logger.exit(task);
        }
        return logger.exit(null);
    }

    private void FollowTaskChainFromFirstTask(BusinessTask firstTask) throws Exception {
        logger.entry();

        if(firstTask.isFollowed()){
            taskList.add(firstTask.getIsFollowedBy());
            FollowTaskChainFromFirstTask(firstTask.getIsFollowedBy());
        }

        logger.exit();
    }
}
