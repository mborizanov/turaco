package com.turaco.dal;

import com.google.common.base.Stopwatch;
import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.sun.media.sound.InvalidDataException;
import com.turaco.domain.BusinessGoal;
import com.turaco.util.Constants;
import com.turaco.util.Global;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Simplifies Data Access operations by wrapping a interface over <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/OntModel.html">Jena OntModel API</a>
 * <p>
 * Costly construction (~500ms) hence BusinessOntology is Singleton.
 *
 * @author Martin Borizanov
 */

public class BusinessOntology {
    private static OntModel ontology;
    private static Logger logger = LogManager.getLogger(ClassLoader.class.getName());
    private static BusinessOntology businessOntology;

    private BusinessOntology() {
        logger.entry();
        Stopwatch sw = Stopwatch.createStarted();
        ontology = getOntologyFromSource();
        logger.info("Ontology was created in: " + sw.elapsed(TimeUnit.MILLISECONDS) + "ms");
        logger.exit();
    }

    /**
     * Re-constructs the BusinessOntology, effectively refreshing the source.
     * <P>
     * To improve performance, the BusinessOntology is "refreshed" only by invoking this method.
     */
    public static void refreshOntology(){
        businessOntology = new BusinessOntology();
    }


    /**
     * Returns the <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a> instance from a local name.
     *
     * @param individualLocalName  The local name of the individual
     * @return The <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a> instance
     */
    public static Individual getIndividualByLocalName(String individualLocalName){
        logger.entry();
        loadBusinessOntology();
        return logger.exit(ontology.getIndividual(Constants.NAME_SPACE + individualLocalName));
    }

    /**
     * Returns the <a href="http://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/OntProperty.html">OntProperty</a> instance when given the property name.
     *
     * @param propertyName  The property name
     * @return <a href="http://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/OntProperty.html">OntProperty</a> instance
     */
    public static OntProperty getOntologyProperty(String propertyName){
        logger.entry();
        loadBusinessOntology();
        return logger.exit(ontology.getOntProperty(Constants.NAME_SPACE + propertyName));
    }

    /**
     * Lists all BusinessGoals contained in the last refreshed loaded ontology (not the currently loaded).
     *
     * @return All BusinessGoal instances within an Ontology
     * @throws InvalidDataException Throws Exception when there are no BusinessGoal instances in the ontology.
     * @throws InvalidDataException Propagated from constructing BusinessGoal instances.
     * @see #refreshOntology()
     * @see com.turaco.domain.BusinessGoal
     */
    public static List<BusinessGoal> listBusinessGoals() throws InvalidDataException {
        logger.entry();

        loadBusinessOntology();
        List<BusinessGoal> businessGoals = new ArrayList<>();

        // Get list of instances
        ExtendedIterator<? extends OntResource> classIterator = ontology.getOntClass(Constants.NAME_SPACE + "BusinessGoal").listInstances();

        while (classIterator.hasNext())
        {
            Individual next = classIterator.next().asIndividual();
            if(isOntologyElement(next.toString()))
            {
                businessGoals.add(new BusinessGoal(next));
            }
        }

        if (businessGoals.size() == 0) throw new InvalidDataException("No business goals in the ontology.");

        return logger.exit(businessGoals);
    }


    private OntModel getOntologyFromSource() {
        logger.entry();

        InputStream ontologySourceFileInputStream = null;

        if(Global.SOURCE == null){
            ontologySourceFileInputStream = getClass().getResourceAsStream(Constants.SOURCE);
        }
        else{
            try {
                ontologySourceFileInputStream = new FileInputStream(Global.SOURCE);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        /*
        Create an ontology ontologyModel with A specification for OWL models that are stored in memory and
        use the OWL rules inference engine for additional entailments
        http://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/OntModelSpec.html
        */
        OntModel testOntology = ModelFactory.createOntologyModel();
        testOntology.read(ontologySourceFileInputStream, "");

        Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
        reasoner = reasoner.bindSchema(testOntology);

        OntModelSpec ontModelSpec = OntModelSpec.OWL_DL_MEM;
        ontModelSpec.setReasoner(reasoner);

        ontology = ModelFactory.createOntologyModel(ontModelSpec,testOntology);

        return logger.exit(ontology);
    }

    private static BusinessOntology loadBusinessOntology(){
        if(businessOntology == null ){
            businessOntology = new BusinessOntology();
        }
        return businessOntology;
    }

    private static boolean isOntologyElement(String element) {
        logger.entry();
        loadBusinessOntology();
        return logger.exit(element.toLowerCase().contains(Constants.NAME_SPACE.toLowerCase()));
    }
}
