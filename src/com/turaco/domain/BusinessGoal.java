package com.turaco.domain;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.sun.media.sound.InvalidDataException;
import com.turaco.dal.BusinessOntology;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * Contains the business logic of the BusinessGoal Turaco ontology class.
 *
 * @author Martin Borizanov
 */
public class BusinessGoal {

    private Individual businessGoal;
    private static Logger logger = LogManager.getLogger(ClassLoader.class.getName());

    /**
     * Constructs a BusinessGoal from a given
     * <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a> instance.
     * <p>
     * Prior to invoking the construction the
     * <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a>
     * is validated as a Business Goal ontology class individual.
     *
     * @param aBusinessGoal
     * @throws InvalidDataException
     */
    public BusinessGoal(Individual aBusinessGoal) throws InvalidDataException {
        logger.entry();

        if(isBusinessGoal(aBusinessGoal)){
            businessGoal = aBusinessGoal;
        } else {
            throw new InvalidDataException("The specified individual is not a member of the BusinessGoal BOWL class!");
        }
    }

    /**
     * Returns the name (effectively the label) of the BusinessGoal instance
     *
     * @return The label of the respective BusinessGoal ontology individual
     */
    public String getName(){
        logger.entry();
        return logger.exit(businessGoal.getLabel(null));
    }

    /**
     * Lists all BusinessMethods that are logically related to this BusinessGoal instance.
     *
     * @return All BusinessMethod instances logically related to this BusinessGoal instance.
     * @throws Exception Propagates exception from the static invocation of BusinessOntology
     * @see BusinessOntology
     */
    public List<BusinessMethod> listBusinessMethods() throws Exception {
        logger.entry();

        List<BusinessMethod> listOfMethods = new ArrayList<>();

        StmtIterator iterator = businessGoal.listProperties();

        while(iterator.hasNext()){
            Statement thisProperty = iterator.next();

            if(thisProperty.getPredicate().getLocalName().equals("hasMethod"))
            {
                listOfMethods.add(new BusinessMethod(BusinessOntology
                        .getIndividualByLocalName(thisProperty.getObject().asResource().getLocalName())));
            }
        }

        return logger.exit(listOfMethods);
    }

    /**
     * Provides a readable String representation of the  BusinessGoal ontology individual
     *
     * @return The label of the respective BusinessGoal ontology individual
     */
    @Override
    public String toString() {
        logger.entry();
        return logger.exit(getName());
    }

    private boolean isBusinessGoal(Individual individual) {
        logger.entry();
        return logger.exit(individual.getOntClass().getLocalName().equals("BusinessGoal"));
    }
}