package com.turaco.domain;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.sun.media.sound.InvalidDataException;
import com.turaco.dal.BusinessOntology;
import com.turaco.util.BusinessTaskIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains the business logic of the BusinessMethod Turaco ontology class.
 *
 * @author Martin Borizanov
 * @see com.turaco.domain.BusinessGoal
 * @see com.turaco.domain.BusinessTask
 */
public class BusinessMethod {

    private Individual businessMethod;
    private static Logger logger = LogManager.getLogger(ClassLoader.class.getName());

    /**
     * Constructs a BusinessMethod from a given
     * <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a> instance.
     * <p>
     * Prior to invoking the construction the
     * <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a>
     * is validated as a Business Method ontology class individual.
     *
     * @param aBusinessMethod  The Business Method Individual
     * @throws Exception Throws exception if the specified individual
     * is not a member of the Business Method Turaco ontology class
     */
    public BusinessMethod(Individual aBusinessMethod) throws Exception {
        logger.entry();

        if (isBusinessMethod(aBusinessMethod)){
            businessMethod = aBusinessMethod;
        } else {
            throw new Exception("The specified individual is not a member of the BusinessMethod Turaco Ontology class!");
        }
    }

    /**
     * Returns the name (effectively the label) of the BusinessMethod instance
     *
     * @return The label of the respective BusinessMethod ontology class individual
     */
    public String getName(){
        logger.entry();
        return logger.exit(businessMethod.getLabel(null));
    }

    /**
     * Returns the comment of the BusinessMethod instance
     *
     * @return The comment of the respective BusinessMethod ontology class individual
     */
    public String getComment(){
        logger.entry();
        return logger.exit(businessMethod.getComment(null));
    }

    /**
     * Lists all BusinessTask instances that are logically related to this BusinessMethod instance.
     *
     * @return All BusinessTask instances logically related to this BusinessMethod instance.
     * @throws Exception Propagates exception from the static invocation of BusinessOntology
     * @see com.turaco.domain.BusinessTask
     */
    public List<BusinessTask> listBusinessTasks() throws Exception {
        logger.entry();

        List<BusinessTask> businessTaskList = new ArrayList<>();
        StmtIterator iterator = businessMethod.listProperties();

        while (iterator.hasNext()){
            Statement thisProperty = iterator.next();

            if (thisProperty.getPredicate().getLocalName().equals("hasTask"))
            {
                businessTaskList.add(new BusinessTask(BusinessOntology
                        .getIndividualByLocalName(thisProperty.getObject().asResource().getLocalName())));
            }

        }

        if (businessTaskList.size() == 0){
            throw new InvalidDataException("This Business Method has no tasks allocated to it!");
        }

        BusinessTaskIterator businessTaskIterator = new BusinessTaskIterator(businessTaskList);
        return logger.exit(businessTaskIterator.toList());
    }

    /**
     * TODO: This is legacy logic, it needs to be refactored.
     * Lists all BusinessGoal instances that are logically related to this BusinessMethod instance.
     *
     * @return All BusinessGoal instances logically related to this BusinessMethod instance.
     * @throws Exception Propagates exception from the static invocation of BusinessOntology
     * @see com.turaco.domain.BusinessGoal
     */
    public List<BusinessGoal> listBusinessGoals() throws Exception {
        logger.entry();

        List<BusinessGoal> businessGoalList = new ArrayList<>();
        StmtIterator iterator = businessMethod.listProperties();

        while (iterator.hasNext()){
            Statement thisProperty = iterator.next();

            if (thisProperty.getPredicate().getLocalName().equals("isMethodOf"))
            {
                businessGoalList.add(new BusinessGoal(BusinessOntology
                    .getIndividualByLocalName(thisProperty.getObject().asResource().getLocalName())));
            }
        }

        return logger.exit(businessGoalList);
    }

    /**
     * Provides a readable String representation of the BusinessMethod ontology individual
     *
     * @return The label of the respective BusinessMethod ontology individual
     */
    @Override
    public String toString() {
        logger.entry();
        return logger.exit(getName());
    }

    private boolean isBusinessMethod(Individual individual) {
        logger.entry();
        return logger.exit(individual.getOntClass().getLocalName().equals("BusinessMethod"));
    }
}
