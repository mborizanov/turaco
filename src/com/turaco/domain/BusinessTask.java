package com.turaco.domain;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.turaco.dal.BusinessOntology;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;

/**
 * Contains the business logic of the BusinessTask Turaco ontology class.
 *
 * @author Martin Borizanov
 * @see com.turaco.domain.BusinessMethod
 * @see com.turaco.domain.BusinessGoal
 * @see com.turaco.util.BusinessTaskIterator
 */
public class BusinessTask {

    private Individual businessTask;
    private Individual individual;
    private OntProperty ontProperty;
    private static Logger logger = LogManager.getLogger(ClassLoader.class.getName());

    /**
     * Constructs a BusinessTask from a given
     * <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a> instance.
     * <p>
     * Prior to invoking the construction the
     * <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a>
     * is validated as a Business Task ontology class individual.
     *
     * @param individual  The Business Task individual
     * @throws Exception Throws exception if the specified individual
     * is not a member of the Business Task Turaco ontology class
     */
    public BusinessTask(Individual individual) throws Exception {
        logger.entry();

        if(isBusinessTask(individual)){
            ontProperty = BusinessOntology.getOntologyProperty("isFollowedBy");
            businessTask = individual;
            this.individual = individual;
        } else {
            throw new Exception(individual.getLabel(null) +  individual.getOntClass().getLocalName() + " is not a member of the BusinessTask BOWL class!");
        }

        logger.exit();
    }

    private boolean isBusinessTask(Individual individual) {
        logger.entry();
        return logger.exit(individual.getOntClass().getLocalName().equals("BusinessTask"));
    }

    /**
     * Returns the name (effectively the label) of the BusinessTask instance
     *
     * @return The label of the respective BusinessTask ontology class individual
     */
    public String getName() {
        logger.entry();
        return logger.exit(businessTask.getLabel(null));
    }

    /**
     * Returns the comment of the BusinessMethod instance
     *
     * @return The comment of the respective BusinessMethod ontology class individual
     */
    public String getComment() {
        logger.entry();
        return logger.exit(businessTask.getComment(null));
    }

    /**
     * @return The Business Method logically related to this Business Task instance
     * @throws InvalidPropertiesFormatException  Throws exception if there is no Business Method
     * instance logically related to this Business Task instance.
     */
    public BusinessMethod getBusinessMethod() throws InvalidPropertiesFormatException {
        logger.entry();

        StmtIterator iterator = businessTask.listProperties();

        while (iterator.hasNext()) {
            Statement thisProperty = iterator.next();

            if (thisProperty.getPredicate().getLocalName().equals("isTaskOf") ) {
                BusinessMethod businessMethod = null;
                try {
                    businessMethod = new BusinessMethod(BusinessOntology.getIndividualByLocalName(thisProperty.getObject().asResource().getLocalName()));
                } catch (Exception e) {
                    logger.catching(e);
                }
                if (businessMethod != null) {
                    return logger.exit(businessMethod);
                }
            }
        }

        throw new InvalidPropertiesFormatException("This business task has no business method!");
    }

    /**
     * @return The name of the Business Method logically related to this Business Task instance
     * @throws InvalidPropertiesFormatException  Throws exception if there is no Business Method
     * instance logically related to this Business Task instance.
     */
    public String getBusinessMethodName() throws InvalidPropertiesFormatException {
        logger.entry();

        StmtIterator iterator = businessTask.listProperties();

        while (iterator.hasNext()) {
            Statement thisProperty = iterator.next();

            if (thisProperty.getPredicate().getLocalName().equals("isTaskOf") ) {
                BusinessMethod businessMethod = null;
                try {
                    businessMethod = new BusinessMethod(BusinessOntology.getIndividualByLocalName(thisProperty.getObject().asResource().getLocalName()));
                } catch (Exception e) {
                    logger.catching(e);
                }
                if (businessMethod != null) {
                    return logger.exit(businessMethod.getName());
                }
            }
        }

        throw new InvalidPropertiesFormatException("This business task has no business method!");
    }

    /**
     * @return The name of the Business Goal logically related to this Business Task instance
     */
    public String getBusinessGoalName(){
        logger.entry();

        try {
            return this.getBusinessMethod().listBusinessGoals().get(0).getName();
        } catch (Exception e) {
            e.printStackTrace();
            logger.catching(e);
        }
        return null;
    }

    /**
     * TODO: This method contains some legacy logic, refactoring is required.
     * Lists all BusinessMethods that are logically related to this Business Task instance.
     *
     * @return All BusinessMethod instances logically related to this Business Task instance.
     * @throws Exception Propagates exception from the static invocation of BusinessOntology
     * @see BusinessOntology
     */
    public List<BusinessMethod> listBusinessMethods() throws Exception {
        logger.entry();

        List<BusinessMethod> businessMethodList = new ArrayList<>();

        StmtIterator iterator = businessTask.listProperties();

        while(iterator.hasNext()){
            Statement thisProperty = iterator.next();

            if(thisProperty.getPredicate().getLocalName().equals("isTaskOf"))
            {
                businessMethodList.add(new BusinessMethod(BusinessOntology
                        .getIndividualByLocalName(thisProperty.getObject().asResource().getLocalName())));
            }
        }

        return logger.exit(businessMethodList);
    }

    /**
     * @return The <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a> instance
     * of this Business Task instance
     */
    public Individual asIndividual(){
        logger.entry();
        return logger.exit(businessTask);
    }


    /**
     * Provides a readable String representation of the  BusinessGoal ontology individual
     *
     * @return The label of the respective BusinessGoal ontology individual
     */
    @Override
    public String toString() {
        logger.entry();
        return logger.exit(getName());
    }

    /**
     * TODO: Duplicated logic
     *
     * @return The <a href="https://jena.apache.org/documentation/javadoc/jena/com/hp/hpl/jena/ontology/Individual.html">Individual</a> instance
     * of this Business Task instance
     */
    public Individual getIndividual() {
        return individual;
    }


    /**
     * @return Boolean flagging whether this Business Task is logically related to another one,
     * via the "isFollowedBy" Turaco property
     */
    public boolean isFollowed(){

        return businessTask.hasProperty(ontProperty);
    }

    /**
     * @return The Business Task instance that is logically related to the current one via the "isFollowedBy" Turaco property .
     */
    public BusinessTask getIsFollowedBy() {

        Statement property = businessTask.getProperty(ontProperty);
        try {
            return new BusinessTask(BusinessOntology.getIndividualByLocalName(property.getObject().asResource().getLocalName()));
        } catch (Exception e) {
            logger.catching(e);
        }
        return null;
    }
}
