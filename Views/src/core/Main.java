package core;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Main extends Application {
    private static Logger logger = LogManager.getLogger(Main.class.getName());

    @Override
    public void start(Stage primaryStage){
        logger.entry();

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("fxml/decomposition.fxml"));
        } catch (IOException e) {
            logger.catching(e);
        }

        Parent welcome = null;
        try {
            welcome = FXMLLoader.load(getClass().getResource("fxml/welcome.fxml"));
        } catch (IOException e) {
            logger.catching(e);
        }


        Parent statistics = null;
        try {
            statistics = FXMLLoader.load(getClass().getResource("fxml/statistics.fxml"));
        } catch (IOException e) {
            logger.catching(e);
        }


        primaryStage.setTitle("Turaco Business Process Decomposition Framework");
        primaryStage.setScene(new Scene(welcome, 1120, 630));
        primaryStage.show();

        logger.exit();
    }


    public static void main(String[] args) {
        logger.entry();
        launch(args);
        logger.exit();
    }
}
