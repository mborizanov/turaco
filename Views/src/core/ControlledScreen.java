package core;

/**
 * Author: Angela Caicedo
 * @see <a href="https://blogs.oracle.com/acaicedo/entry/managing_multiple_screens_in_javafx1">Angela's Blog</a>
 */
public interface ControlledScreen {
    public void setScreenParent(ScreensController screenPage);
}
