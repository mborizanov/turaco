package core.controller;

import com.turaco.dal.BusinessOntology;
import com.turaco.domain.BusinessGoal;
import com.turaco.domain.BusinessMethod;
import com.turaco.util.Global;
import core.ControlledScreen;
import core.ScreensController;
import core.ScreensFramework;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ResourceBundle;

public class WelcomeController implements ControlledScreen {

    @FXML private Label statisticsLabel;
    @FXML private Label statusLabel;
    @FXML private ResourceBundle resources;
    @FXML private URL location;
    @FXML private Button processDecompositionButton;
    @FXML private ImageView logoImageView;
    @FXML private Button loadOntologyButton;
    @FXML private Button managerialStatisticsButton;

    private int numberOfMethods = 0;
    private int numberOfTasks = 0;
    private double averageNumberOfTasksPerMethod = 0;
    private ScreensController myController;

    /**
     * Author: Angela Caicedo
     * @see <a href="https://blogs.oracle.com/acaicedo/entry/managing_multiple_screens_in_javafx1">Angela's Blog</a>
     */
    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }

    @FXML
    void initialize() {
        assert processDecompositionButton != null : "fx:id=\"processDecompositionButton\" was not injected: check your FXML file 'welcome.fxml'.";
        assert logoImageView != null : "fx:id=\"logoImageView\" was not injected: check your FXML file 'welcome.fxml'.";
        assert loadOntologyButton != null : "fx:id=\"loadOntologyButton\" was not injected: check your FXML file 'welcome.fxml'.";
        assert managerialStatisticsButton != null : "fx:id=\"managerialStatisticsButton\" was not injected: check your FXML file 'welcome.fxml'.";



        processDecompositionButton.setDisable(true);
        managerialStatisticsButton.setDisable(true);
        statusLabel.setTextFill(Color.GRAY);
        statisticsLabel.setTextFill(Color.GRAY);

        URL imageSrc = getClass().getResource("/largeLogo.png");
        Image image = new Image(String.valueOf(imageSrc));
        logoImageView.setImage(image);
    }

    @FXML
    private void handleLoadingOntology(){

        numberOfMethods = 0;
        numberOfTasks = 0;
        averageNumberOfTasksPerMethod = 0;

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Ontology");
        File file = fileChooser.showOpenDialog(null);

        if(file!=null){
            BusinessOntology.refreshOntology();
            System.out.println(file.getAbsoluteFile().getAbsolutePath());
            Global.SOURCE = file.getAbsoluteFile().getAbsolutePath();
        }

        calculateStatistics();
        processDecompositionButton.setDisable(false);
        managerialStatisticsButton.setDisable(false);
        statusLabel.setTextFill(Color.BLACK);
        statisticsLabel.setTextFill(Color.BLACK);

        statusLabel.setText("Ontology loaded!");
        DecimalFormat f = new DecimalFormat("##.00");
        statisticsLabel.setText(String.format("Methods: %d;    Tasks: %d;    Average Number of Tasks: %s;", numberOfMethods, numberOfTasks, f.format(averageNumberOfTasksPerMethod)));
    }

    @FXML
    private void goToDecomposition(){
        myController.setScreen(ScreensFramework.DECOMPOSITION_SCREEN);
    }

    @FXML
    private void goToStatistics(){
        myController.setScreen(ScreensFramework.STATISTICS_SCREEN);
    }

    private void calculateStatistics() {
        try {
            BusinessOntology.refreshOntology();
            List<BusinessGoal> businessGoals = BusinessOntology.listBusinessGoals();
            for(BusinessGoal businessGoal : businessGoals){
                numberOfMethods += businessGoal.listBusinessMethods().size();
                List<BusinessMethod> businessMethods = businessGoal.listBusinessMethods();
                for(BusinessMethod businessMethod : businessMethods){
                    numberOfTasks += businessMethod.listBusinessTasks().size();
                }
            }
            averageNumberOfTasksPerMethod = (double) numberOfTasks / (double) numberOfMethods;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}



