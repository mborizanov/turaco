package core.controller;

import com.turaco.dal.BusinessOntology;
import com.turaco.domain.BusinessGoal;
import com.turaco.domain.BusinessMethod;
import com.turaco.domain.BusinessTask;
import core.ControlledScreen;
import core.ScreensController;
import core.ScreensFramework;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class StatisticsController implements ControlledScreen {

    @FXML private ImageView logoImageView;
    @FXML private Button bmPieChartButton;
    @FXML private Button bmLineChartButton;
    @FXML private Button bmBarChartButton;
    @FXML private Button lineChartButton;
    @FXML private Button pieChartButton;
    @FXML private Button goBackButton;
    @FXML private Button barChartButton;
    @FXML private ComboBox<BusinessGoal> busGoalComboBox;
    @FXML private ListView<String> busMethodListView;
    @FXML private TextField busMethodTxtField;
    @FXML private ResourceBundle resources;
    @FXML private URL location;
    @FXML private BarChart<?, ?> barChart;
    @FXML private LineChart<Number, Number> lineChart;
    @FXML private PieChart pieChart;

    private ScreensController myController;
    private int numberOfMethods = 0;
    private int numberOfTasks = 0;
    private double averageNumberOfTasksPerMethod = 0;
    private List<BusinessGoal> businessGoals;
    private List<BusinessTask> cachedBusinessTasks;
    private List<BusinessMethod> cachedBusinessMethods;
    private List<String> cachedBusinessMethodsNames = new ArrayList<>();
    private BusinessMethod selectedBusinessMethod;
    private BusinessGoal selectedBusinessGoal;
    private static Logger logger = LogManager.getLogger(DecompositionController.class.getName());

    /**
     * Author: Angela Caicedo
     * @see <a href="https://blogs.oracle.com/acaicedo/entry/managing_multiple_screens_in_javafx1">Angela's Blog</a>
     */
    public void setScreenParent(ScreensController screenParent){
        logger.entry();
        myController = screenParent;
        logger.exit();
    }

    @FXML
    private void initialize() {
        assert barChart != null : "fx:id=\"barChart\" was not injected: check your FXML file 'statistics.fxml'.";
        assert lineChartButton != null : "fx:id=\"lineChartButton\" was not injected: check your FXML file 'statistics.fxml'.";
        assert pieChartButton != null : "fx:id=\"pieChartButton\" was not injected: check your FXML file 'statistics.fxml'.";
        assert goBackButton != null : "fx:id=\"goBackButton\" was not injected: check your FXML file 'statistics.fxml'.";
        assert lineChart != null : "fx:id=\"lineChart\" was not injected: check your FXML file 'statistics.fxml'.";
        assert barChartButton != null : "fx:id=\"barChartButton\" was not injected: check your FXML file 'statistics.fxml'.";
        assert pieChart != null : "fx:id=\"pieChart\" was not injected: check your FXML file 'statistics.fxml'.";

        busGoalComboBox.setDisable(true);
        busMethodListView.setDisable(true);
        busMethodTxtField.setDisable(true);

        barChart.setVisible(false);
        pieChart.setVisible(false);
        lineChart.setVisible(false);
        lineChart.setAnimated(false);
        barChart.setAnimated(false);
        pieChart.setAnimated(false);

        bmBarChartButton.setDisable(true);
        bmLineChartButton.setDisable(true);
        bmPieChartButton.setDisable(true);
        lineChartButton.setDisable(true);
        barChartButton.setDisable(true);
        pieChartButton.setDisable(true);

        calculateStatistics();

        URL imageSrc = getClass().getResource("/logo.png");
        Image image = new Image(String.valueOf(imageSrc));
        logoImageView.setImage(image);

        busMethodTxtField.setPromptText("Search");
        busMethodTxtField.textProperty().addListener(new ChangeListener() {
            public void changed(ObservableValue observable, Object oldVal,
                                Object newVal) {
                search((String) oldVal, (String) newVal);
            }
        });
        busMethodListView.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<String>() {
                    public void changed(ObservableValue<? extends String> ov,
                                        String old_val, String new_val) {
                        if(old_val!=new_val)
                        handleBusinessMethodsListView();
                    }
                });

    }

    @FXML
    private void handleBusinessGoalsComboBox() {
        logger.entry();

        selectedBusinessGoal = busGoalComboBox.getSelectionModel().getSelectedItem();
        List<BusinessMethod> businessMethods = null;

        lineChartButton.setDisable(false);
        barChartButton.setDisable(false);
        pieChartButton.setDisable(false);

        cachedBusinessMethodsNames.clear();
        try {
            businessMethods = selectedBusinessGoal.listBusinessMethods();
            cachedBusinessMethods = businessMethods;
            for(BusinessMethod businessMethod : businessMethods) {
                cachedBusinessMethodsNames.add(businessMethod.toString());
            }
        } catch (Exception e) {
            logger.catching(e);
        }

        busMethodListView.getItems().clear();

        if (businessMethods != null) {
            for (BusinessMethod businessMethod : businessMethods) {
                busMethodListView.getItems().add(businessMethod.toString());
            }
        }

        busMethodTxtField.setDisable(false);
        busMethodListView.setDisable(false);

        logger.exit();
    }

    @FXML
    private void handleBusinessGoalsBarChartButton()  {
        logger.entry();

        barChart.setVisible(true);
        pieChart.setVisible(false);
        lineChart.setVisible(false);
        barChart.getData().clear();
        lineChart.getData().clear();
        pieChart.getData().clear();

        if(selectedBusinessGoal != null){
            final CategoryAxis xAxis = new CategoryAxis();
            final NumberAxis yAxis = new NumberAxis();
            XYChart.Series series2 = new XYChart.Series();

            barChart.setTitle(selectedBusinessGoal.getName() + " Summary");
            xAxis.setLabel("Variable");
            yAxis.setLabel("Value");

            List<BusinessMethod> businessMethods = null;
            try {
                businessMethods = selectedBusinessGoal.listBusinessMethods();
            } catch (Exception e) {
                logger.catching(e);
            }

            series2.getData().add(new XYChart.Data("Average # of Tasks", averageNumberOfTasksPerMethod));
            series2.setName("Average # of Tasks");
            barChart.getData().add(series2);

            for(BusinessMethod businessMethod : businessMethods){
                XYChart.Series series = new XYChart.Series();
                series.setName(businessMethod.getName());
                try {
                    series.getData().add(new XYChart.Data("# of Tasks", businessMethod.listBusinessTasks().size()));
                } catch (Exception e) {
                    logger.catching(e);
                }
                barChart.getData().add(series);
            }
        }

        logger.exit();
    }

    @FXML
    private void handleBusinessGoalsLineChartButton()  {
        logger.entry();

        barChart.setVisible(false);
        pieChart.setVisible(false);
        lineChart.setVisible(true);
        barChart.getData().clear();
        lineChart.getData().clear();
        pieChart.getData().clear();

        if(selectedBusinessGoal != null){
            final CategoryAxis xAxis = new CategoryAxis();
            final NumberAxis yAxis = new NumberAxis();

            lineChart.setTitle(selectedBusinessGoal.getName() + " Summary");
            xAxis.setLabel("Variable");
            yAxis.setLabel("Value");

            List<BusinessMethod> businessMethods = null;
            try {
                businessMethods = selectedBusinessGoal.listBusinessMethods();
            } catch (Exception e) {
                logger.catching(e);
            }

            if (businessMethods != null) {
                for(BusinessMethod businessMethod : businessMethods){
                    XYChart.Series series = new XYChart.Series();
                    series.setName(businessMethod.getName());
                    try {
                        series.getData().add(new XYChart.Data("# of Operational Tasks", businessMethod.listBusinessTasks().size()));
                    } catch (Exception e) {
                        logger.catching(e);
                    }
                    series.getData().add(new XYChart.Data("Average # of Tasks", averageNumberOfTasksPerMethod));
                    lineChart.getData().add(series);
                }
            }
        }

        logger.exit();
    }

    @FXML
    private void handleBusinessGoalsPieChartButton()  {
        logger.entry();

        barChart.setVisible(false);
        pieChart.setVisible(true);
        lineChart.setVisible(false);
        barChart.getData().clear();
        lineChart.getData().clear();
        pieChart.getData().clear();

        if(selectedBusinessGoal != null){
            pieChart.setTitle(selectedBusinessGoal.getName() + " Summary");

            ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList( );

            List<BusinessMethod> businessMethods = null;
            try {
                businessMethods = selectedBusinessGoal.listBusinessMethods();
            } catch (Exception e) {
                logger.catching(e);
            }
            if (businessMethods != null) {
                for(BusinessMethod businessMethod : businessMethods){
                    try {
                        pieChartData.add(new PieChart.Data(businessMethod.getName(),businessMethod.listBusinessTasks().size()));
                    } catch (Exception e) {
                        logger.catching(e);
                    }
                }
            }

            pieChart.getData().addAll(pieChartData);
        }

        logger.exit();
    }

    @FXML
    private void handleBusinessMethodsBarChart(ActionEvent actionEvent) {
        logger.entry();

        barChart.setVisible(true);
        pieChart.setVisible(false);
        lineChart.setVisible(false);
        barChart.getData().clear();
        lineChart.getData().clear();
        pieChart.getData().clear();

        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();

        barChart.setTitle(selectedBusinessMethod.getName() + " Summary");
        xAxis.setLabel("Variable");
        yAxis.setLabel("Value");

        if(selectedBusinessMethod != null){
            for(BusinessTask businessTask : cachedBusinessTasks){
                XYChart.Series series = new XYChart.Series();
                series.setName(businessTask.getName());
                series.getData().add(new XYChart.Data("Task Size", 1));
                barChart.getData().add(series);
            }

        }

        logger.entry();
    }

    @FXML
    private void handleBusinessMethodsPieChart(ActionEvent actionEvent) {
        logger.entry();

        barChart.setVisible(false);
        pieChart.setVisible(true);
        lineChart.setVisible(false);
        barChart.getData().clear();
        lineChart.getData().clear();
        pieChart.getData().clear();

        pieChart.setTitle(selectedBusinessMethod.getName() + " Summary");

        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList( );

        if(selectedBusinessMethod != null){
            for(BusinessTask businessTask : cachedBusinessTasks){
                pieChartData.add(new PieChart.Data(businessTask.getName(), 1));
            }
        }

        pieChart.getData().addAll(pieChartData);

        logger.exit();
    }

    @FXML
    private void handleBusinessMethodsLineChart(ActionEvent actionEvent) {
        logger.entry();

        barChart.setVisible(false);
        pieChart.setVisible(false);
        lineChart.setVisible(true);
        barChart.getData().clear();
        lineChart.getData().clear();
        pieChart.getData().clear();

        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        lineChart.setTitle(selectedBusinessMethod.getName() + " Summary");
        xAxis.setLabel("Variable");
        yAxis.setLabel("Value");

        if(selectedBusinessMethod != null){
            for(BusinessTask businessTask : cachedBusinessTasks){
                XYChart.Series series = new XYChart.Series();
                series.setName(businessTask.getName());
                series.getData().add(new XYChart.Data(businessTask.getName(), 1));
                series.getData().add(new XYChart.Data("Average Number of Tasks", averageNumberOfTasksPerMethod));
                lineChart.getData().add(series);
            }

        }

        logger.exit();
    }

    @FXML
    private void goBack(){
        logger.entry();
        myController.setScreen(ScreensFramework.MAIN_SCREEN);
        logger.exit();
    }

    @FXML
    private void refreshSchema(ActionEvent actionEvent) {
        logger.entry();

        busGoalComboBox.setDisable(false);
        busMethodListView.setDisable(false);
        busMethodTxtField.setDisable(false);
        busMethodTxtField.clear();
        busGoalComboBox.getItems().clear();

        distillOntologyData();
        busGoalComboBox.getItems().addAll(businessGoals);

        logger.exit();
    }

    private void search(String oldVal, String newVal) {
        logger.entry();

        if (oldVal != null && (newVal.length() < oldVal.length())) {
            busMethodListView.getItems().clear();
            for(BusinessMethod businessMethod : cachedBusinessMethods)
                busMethodListView.getItems().add(businessMethod.toString());
        }
        String value = newVal.toUpperCase();
        ObservableList<String> subEntries = FXCollections.observableArrayList();
        for (String entry : busMethodListView.getItems()) {
            boolean match = true;
            if (!entry.toUpperCase().contains(value)) {
                match = false;
            }
            if (match) {
                subEntries.add(entry);
            }
        }
        busMethodListView.setItems(subEntries);

        logger.exit();
    }

    private void handleBusinessMethodsListView() {
        logger.entry();

        bmBarChartButton.setDisable(false);
        bmLineChartButton.setDisable(false);
        bmPieChartButton.setDisable(false);

        cachedBusinessMethodsNames.clear();
        for(BusinessMethod businessMethod : cachedBusinessMethods){
            cachedBusinessMethodsNames.add(businessMethod.toString());
        }

        String businessMethodName = busMethodListView.getSelectionModel().getSelectedItem();
        int index = cachedBusinessMethodsNames.indexOf(businessMethodName);

        if(index > -1){
            selectedBusinessMethod = cachedBusinessMethods.get(index);
            if(selectedBusinessMethod != null){
                List<BusinessTask> businessTasks;
                try {
                    businessTasks = selectedBusinessMethod.listBusinessTasks();
                    cachedBusinessTasks = businessTasks;
                } catch (Exception e) {
                    logger.catching(e);
                }
            }
        }

        logger.exit();
    }

    private void distillOntologyData(){
        logger.entry();

        try {
            BusinessOntology.refreshOntology();
            busGoalComboBox.getItems().clear();
            businessGoals = BusinessOntology.listBusinessGoals();
        } catch (Exception e) {
            logger.catching(e);
        }

        logger.exit();
    }

    private void calculateStatistics() {
        logger.entry();

        try {
            List<BusinessGoal> businessGoals = BusinessOntology.listBusinessGoals();
            for(BusinessGoal businessGoal : businessGoals){
                numberOfMethods += businessGoal.listBusinessMethods().size();
                List<BusinessMethod> businessMethods = businessGoal.listBusinessMethods();
                for(BusinessMethod businessMethod : businessMethods){
                    numberOfTasks += businessMethod.listBusinessTasks().size();
                }
            }
            averageNumberOfTasksPerMethod = (double) numberOfTasks / (double) numberOfMethods;
        } catch (Exception e) {
            logger.catching(e);
        }

        logger.exit();
    }
}
