package core.controller;

import com.google.common.base.Stopwatch;
import com.turaco.domain.BusinessGoal;
import com.turaco.domain.BusinessMethod;
import com.turaco.dal.BusinessOntology;
import com.turaco.domain.BusinessTask;
import com.turaco.util.BusinessTaskIterator;
import com.turaco.util.OperationalTaskDiagram;
import com.turaco.util.OperationalTasksReport;
import core.ControlledScreen;
import core.ScreensController;
import core.ScreensFramework;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.turaco.util.OperationalTaskDiagram.exportToPng;

public class DecompositionController implements ControlledScreen {

    @FXML private Button saveAsPngButton;
    @FXML private Button saveAsPdfButton;
    @FXML private Button displayButton;
    @FXML private VBox tasksVBox;
    @FXML private ScrollPane tasksScrollPane;
    @FXML private TextField txtField;
    @FXML private ListView<String> listView;
    @FXML private ImageView logoImage;
    @FXML private ComboBox<BusinessGoal> businessGoalsComboBox;
    @FXML private Label businessGoalsLabel;
    @FXML private Label businessMethodLabel;

    private ScreensController myController;
    private List<BusinessGoal> businessGoals;
    private List<BusinessTask> cachedBusinessTasks;
    private List<BusinessMethod> cachedBusinessMethods;
    private List<String> cachedBusinessMethodsNames;
    private static Logger logger = LogManager.getLogger(DecompositionController.class.getName());
    private OperationalTaskDiagram diagram;

    /**
     * Author: Angela Caicedo
     * @see <a href="https://blogs.oracle.com/acaicedo/entry/managing_multiple_screens_in_javafx1">Angela's Blog</a>
     */
    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }


    @FXML
    void initialize() {
        logger.entry();

        performInjectionChecks();

        txtField.setPromptText("Search");
        businessGoalsComboBox.setDisable(true);
        txtField.setDisable(true);
        listView.setDisable(true);
        saveAsPngButton.setDisable(true);
        displayButton.setDisable(true);
        saveAsPdfButton.setDisable(true);
        tasksVBox.setPadding(new Insets(20));
        URL imageSrc = getClass().getResource("/logo.png");

        Image image = new Image(String.valueOf(imageSrc));
        logoImage.setImage(image);

        txtField.textProperty().addListener(new ChangeListener() {
            public void changed(ObservableValue observable, Object oldVal,
                                Object newVal) {
                search((String) oldVal, (String) newVal);
            }
        });
        listView.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<String>() {
                    public void changed(ObservableValue<? extends String> ov,
                                        String old_val, String new_val) {
                        handleBusinessMethods();
                    }
                });

        logger.exit();
    }

    @FXML
    private void handleToggleButton(ActionEvent actionEvent) {
        logger.entry();

        Stopwatch sw = Stopwatch.createStarted();
        diagram = new OperationalTaskDiagram(cachedBusinessTasks);
        diagram.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        diagram.setSize(diagram.getDiagramWidth(), diagram.getDiagramHeight());
        diagram.setVisible(true);
        logger.info("Diagram generated in: " + sw.elapsed(TimeUnit.MILLISECONDS) + "ms");

        logger.exit();
    }

    @FXML
    private void saveAsPDF(ActionEvent actionEvent) {
        logger.entry();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Report as PDF");
        File file = fileChooser.showSaveDialog(null);
        if(file!=null){
            OperationalTasksReport report = new OperationalTasksReport(cachedBusinessTasks,file.getAbsolutePath());
        }

        logger.exit();
    }

    @FXML
    private void saveAsPNG(ActionEvent actionEvent) {
        logger.entry();
        diagram = new OperationalTaskDiagram(cachedBusinessTasks);
        diagram.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        diagram.setSize(diagram.getDiagramWidth(), diagram.getDiagramHeight());
        diagram.setVisible(true);
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Diagram as PNG");
        File file = fileChooser.showSaveDialog(null);
        if(file!=null){
            exportToPng(diagram, file.getAbsolutePath());
        }
        diagram.dispose();
        logger.exit();
    }

    @FXML
    private void goBack(){
        myController.setScreen(ScreensFramework.MAIN_SCREEN);
    }

    @FXML
    private void refreshSchema(ActionEvent actionEvent) {
        logger.entry();

        businessGoalsComboBox.setDisable(false);
        distillOntologyData();
        businessGoalsComboBox.getItems().addAll(businessGoals);

        logger.exit();
    }

    @FXML
    private void handleBusinessGoalComboBoxAction() {
        logger.entry();
        cachedBusinessMethodsNames = new ArrayList<>();
        BusinessGoal businessGoal = businessGoalsComboBox.getSelectionModel().getSelectedItem();
        List<BusinessMethod> businessMethods = null;
        try {
            Stopwatch sw = Stopwatch.createStarted();
            businessMethods = businessGoal.listBusinessMethods();
            cachedBusinessMethods = businessMethods;
            for(BusinessMethod businessMethod : businessMethods)
                cachedBusinessMethodsNames.add(businessMethod.toString());
            logger.info("Business Methods were returned in: " + sw.elapsed(TimeUnit.MILLISECONDS) + "ms");
        } catch (Exception e) {
            logger.catching(e);
        }


        listView.getItems().clear();
        for(int i = 0; i<businessMethods.size(); i++){
            Stopwatch sw = Stopwatch.createStarted();
            listView.getItems().add(businessMethods.get(i).toString());
            logger.info("Business Methods CBox was populated in: " + sw.elapsed(TimeUnit.MILLISECONDS) + "ms");
        }

        txtField.setDisable(false);
        listView.setDisable(false);

        logger.exit();
    }


    private void handleBusinessMethods() {
        logger.entry();

        String businessMethodName = listView.getSelectionModel().getSelectedItem();
        int index = cachedBusinessMethodsNames.indexOf(businessMethodName);

        if(index > -1){
            BusinessMethod businessMethod = cachedBusinessMethods.get(index);

            if(businessMethod != null){
                List<BusinessTask> businessTasks = null;

                try {
                    Stopwatch sw = Stopwatch.createStarted();
                    businessTasks = businessMethod.listBusinessTasks();
                    logger.info("Business Tasks were generated in: " + sw.elapsed(TimeUnit.MILLISECONDS) + "ms");
                    cachedBusinessTasks = businessTasks;
                } catch (Exception e) {
                    logger.catching(e);
                }

                int i = 1;
                tasksVBox.getChildren().clear();

                try{
                    BusinessTaskIterator iterator = new BusinessTaskIterator(businessTasks);
                    while(iterator.hasNext()){
                        BusinessTask next = iterator.next();
                        Text taskText = new Text(i + " " + next);
                        taskText.setFont(Font.font("Helvetica", 16));
                        tasksVBox.getChildren().add(taskText);
                        Text comment = new Text(next.getComment() + "\n");
                        comment.setFont(Font.font("Helvetica", 12));
                        comment.setFill(Color.web("#555"));
                        comment.setWrappingWidth(600);
                        tasksVBox.getChildren().add(comment);
                        i++;
                    }
                } catch (Exception e) {
                    logger.catching(e);
                }

                saveAsPngButton.setDisable(false);
                displayButton.setDisable(false);
                saveAsPdfButton.setDisable(false);
            }
        }
        logger.exit();
    }

    private void performInjectionChecks() {
        if (tasksScrollPane == null) logger.error("fx:id=\"tasksScrollPane\" was not injected: check your FXML file 'decomposition.fxml'.");
        if (logoImage == null) logger.error("fx:id=\"logoImage\" was not injected: check your FXML file 'decomposition.fxml'.");
        if (businessGoalsComboBox == null) logger.error("fx:id=\"businessGoalsComboBox\" was not injected: check your FXML file 'decomposition.fxml'.");
        if (tasksVBox == null) logger.error("fx:id=\"tasksVBox\" was not injected: check your FXML file 'decomposition.fxml'.");
        if (saveAsPdfButton == null) logger.error("fx:id=\"saveAsPdfButton\" was not injected: check your FXML file 'decomposition.fxml'.");
        if (businessGoalsLabel == null) logger.error("fx:id=\"businessGoalsLabel\" was not injected: check your FXML file 'decomposition.fxml'.");
        if (businessMethodLabel == null) logger.error("fx:id=\"businessMethodLabel\" was not injected: check your FXML file 'decomposition.fxml'.");
        if (saveAsPngButton == null) logger.error("fx:id=\"saveAsPngButton\" was not injected: check your FXML file 'decomposition.fxml'.");
        if (displayButton == null) logger.error("fx:id=\"displayButton\" was not injected: check your FXML file 'decomposition.fxml'.");
    }

    private void distillOntologyData(){
        logger.entry();

        try {
            BusinessOntology.refreshOntology();
            businessGoalsComboBox.getItems().clear();
            businessGoals = BusinessOntology.listBusinessGoals();
        } catch (Exception e) {
            logger.catching(e);
            e.printStackTrace();
        }

        logger.exit();
    }

    private void search(String oldVal, String newVal) {
        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listView.getItems().clear();
            for(BusinessMethod businessMethod : cachedBusinessMethods)
                listView.getItems().add(businessMethod.toString());
        }
        String value = newVal.toUpperCase();
        ObservableList<String> subEntries = FXCollections.observableArrayList();
        for (String entry : listView.getItems()) {
            boolean match = true;
            String entryText = entry;
            if (!entryText.toUpperCase().contains(value)) {
                match = false;
            }
            if (match) {
                subEntries.add(entryText);
            }
        }
        listView.setItems(subEntries);
    }
}


