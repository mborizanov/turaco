package core;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Author: Angela Caicedo
 * @see <a href="https://blogs.oracle.com/acaicedo/entry/managing_multiple_screens_in_javafx1">Angela's Blog</a>
 */
public class ScreensFramework extends Application {

    public static final String MAIN_SCREEN = "main";
    public static final String MAIN_SCREEN_FXML = "fxml/welcome.fxml";
    public static final String STATISTICS_SCREEN = "statistics";
    public static final String STATISTICS_FXML = "fxml/statistics.fxml";
    public static final String DECOMPOSITION_SCREEN = "decomposition";
    public static final String DECOMPOSITION_FXML = "fxml/decomposition.fxml";

    @Override
    public void start(Stage primaryStage) {

        ScreensController mainContainer = new ScreensController();
        mainContainer.loadScreen(ScreensFramework.MAIN_SCREEN,ScreensFramework.MAIN_SCREEN_FXML);
        mainContainer.loadScreen(ScreensFramework.STATISTICS_SCREEN,ScreensFramework.STATISTICS_FXML);
        mainContainer.loadScreen(ScreensFramework.DECOMPOSITION_SCREEN,ScreensFramework.DECOMPOSITION_FXML);

        mainContainer.setScreen(ScreensFramework.MAIN_SCREEN);

        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}